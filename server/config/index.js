/**
 * Created by Jingjie on 14/11/16.
 */

'use strict';

var ENV = process.env.NODE_ENV || "development";

module.exports = require('./' + ENV + '.js') || {};