/**
 * Created by Jingjie on 14/11/16.
 */

var express = require("express");
var bodyParser = require('body-parser');
var passport = require("passport");
var session = require("express-session");
var flash = require('connect-flash');
var config = require("./config");

var app = express();

app.use(flash());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(session({
    secret: "1111",
    resave: false,
    saveUninitialized: true,
    maxAge: 20000
}));

app.use(passport.initialize());
app.use(passport.session());


require("./auth.js")(app, passport);
var routes = require("./routes")(app, passport);

//routes(app);

app.use(express.static(__dirname + "/../client/"));
app.use("/bower_components", express.static(__dirname + "/../client/bower_components"));



app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Web Application started on port %d", app.get("port"));
});