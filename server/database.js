/**
 * Created by Jingjie on 14/11/16.
 */
var Sequelize = require ( "sequelize");
var config = require('./config');

var sequelize = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

// sequelize.sync({})
//     .then(function(){
//         console.log("Database Sync-ed");
//     }).catch (function(err){
//     console.log("Database Sync failed");
//     console.log(err);
// });

var Dish = require("./api/dishes/dishes.model")(sequelize, Sequelize);
var Order = require("./api/orders/orders.model")(sequelize, Sequelize);
var OrderDetails = require("./api/orders/order_details.model")(sequelize, Sequelize);
var Review = require("./api/reviews/reviews.model")(sequelize, Sequelize);
var User = require("./api/users/users.model")(sequelize, Sequelize);


User.hasMany(Dish,  {
    foreignKey: {
        name: 'user_id',
        allowNull: false
    },
    targetKey: 'id'
});
User.hasMany(Review,  {
    foreignKey: {
        name: 'buy_user_id',
        allowNull: false
    },
    targetKey: 'id'
});
User.hasMany(Review,  {
    foreignKey: {
        name: 'sell_user_id',
        allowNull: false
    },
    targetKey: 'id'
});
Review.belongsTo(User, {
    foreignKey: {
        name: 'sell_user_id',
        allowNull: false
    },
    targetKey: 'id'
});
Review.belongsTo(User, {
    foreignKey: {
        name: 'buy_user_id',
        allowNull: false
    },
    targetKey: 'id'
});
Order.hasMany(OrderDetails,  {
    foreignKey: {
        name: 'order_id',
        allowNull: false
    },
    targetKey: 'id'
});
//User.hasMany(Order);
//Dish.hasMany(Order);


module.exports = {
    Dish: Dish,
    Order: Order,
    OrderDetails: OrderDetails,
    Review: Review,
    User: User
};