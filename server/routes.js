/**
 * Created by Jingjie on 14/11/16.
 */
'use strict';

var AWSController = require("./api/AWS/aws.controller");
var DishController = require("./api/dishes/dishes.controller");
var OrderController = require("./api/orders/orders.controller");
var ReviewController = require("./api/reviews/reviews.controller");
var UserController = require("./api/users/users.controller");
var express = require("express");
var config = require("./config");

module.exports = function (app, passport) {

    app.use(express.static(__dirname + "/../client/"));

    //app.get('/api/aws/createS3Policy', PostController.list);
    app.post('/api/aws/s3-policy', AWSController.getSignedPolicy);

    app.get('/api/users', UserController.list);
    app.post('/api/users', UserController.create);
    app.get('/api/users/:id', UserController.search);
    app.put('/api/users/:id', UserController.update);
    app.delete('/api/users/:id', UserController.delete);
    app.get('/api/allUsers/:id', UserController.searchAll);

    app.post('/api/dishes', DishController.create);
    app.get('/api/dish/:id', DishController.dish);
    app.get('/api/dishes/:id', DishController.list);
    app.delete('/api/dishes/:id', DishController.delete);
    app.put('/api/dishes/:id', DishController.update);

    app.post('/api/reviews', ReviewController.create);
    app.get('/api/reviews/:id', ReviewController.list);

    app.post('/api/order/:id', OrderController.create);
    app.get('/api/order/:id', OrderController.listbuy);
    app.put('/api/order/:id', OrderController.update);
    app.post('/api/orders/:id', OrderController.details);
    app.get('/api/orders/:id', OrderController.listAll);
    app.put('/api/orders/:id', OrderController.payment);
    app.get('/api/sellOrders/:id', OrderController.listsell);

    app.get("/status/user", function (req, res) {
        var status = "";
        console.log("routes l30");
        console.log(req.user);
        if(req.user) {
            status = req.user.name_first;
        }
        console.info("status of the user --> " + status);

        res.send(status).end();
    });

    app.post("/login", passport.authenticate("local", {
        successRedirect: "/success",
        failureRedirect: "/",
        failureFlash : true
    }));


    app.use("/success", function(req, res){
        console.log("success");
        console.log(req.user);
        res.send(req.user);
    });

    app.get("/logout", function(req, res) {
        req.logout();
        req.session.destroy();
        res.send(req.user).end();
    });

    app.get("/checkauth", isAuthenticated);

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated()){
            res.send({ key: true, user : req.user});
        }else {
            res.send({key : false});
        }
    }

};


