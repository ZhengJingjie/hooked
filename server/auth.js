/**
 * Created by Jingjie on 23/11/16.
 */
var localStrategy = require("passport-local").Strategy;

var User = require("./database").User;
var config = require("./config");

module.exports = function (app, passport) {
    var authenticate = function (username, password, done) {
        console.log("authenticating- ");

        User.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if(!result){
                return done(null, false);
            }else{
                if(password == result.password){
                    console.log("Match");
                    return done(null, result);
                }else{
                    console.log("Not Match");
                    return done(null, false);
                }
            }
        }).catch(function(err){
            return done(err, false);
        });
    };

    passport.use(new localStrategy ({
            usernameField: "email",
            passwordField: "password"
        },
        authenticate));

    passport.serializeUser(function (user, done) {
        console.info("in session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            done(err, user);
        });
    });
}