/**
 * Created by Jingjie on 21/11/16.
 */
module.exports = function (sequelize, Sequelize){
    var Orders = sequelize.define('orders', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        pick_up: {
            type: Sequelize.DATE,
            allowNull: true
        },
        order_no: {
            type: Sequelize.STRING,
            allowNull: false
        },
        total_bill: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        buy_user_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        sell_user_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        confirmed: {
            type: Sequelize.STRING,
            allowNull: true
        },
        completed: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
        tableName: 'orders',
        timestamp : true
    });

    return Orders;
};