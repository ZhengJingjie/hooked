/**
 * Created by Jingjie on 21/11/16.
 */
var Order = require("../../database").Order;
var OrderDetails = require("../../database").OrderDetails;
var paypal_api = require('paypal-rest-sdk');
var config = require("../../config");

exports.create = function (req, res) {
    console.log("inside server controller");
    Order
        .create({
            order_no: req.body.order_no,
            buy_user_id: req.body.buy_user_id,
            sell_user_id: req.body.sell_user_id,
            confirmed: req.body.confirmed,
            completed: req.body.completed,
            total_bill: req.body.total_bill
        })
        .then(function (order) {
            res.json(order);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.listbuy = function (req, res) {
    Order
        .findAll({
            where: {
                order_no: req.params.id,
                confirmed: "N"
            },
            include: [{
                model: OrderDetails
            }]
        })
        .then(function (orders) {
            res.json(orders);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function(req,res){
    Order
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                completed: req.body.completed
            }).then(function (){
                console.log("Updated profile");
                res.status(200).end();
            }).catch(function (err){
                console.log("Update failed");
                handleErr(res, err);
            });
        })
        .catch(function(err){
            handleErr(res, err);
        });

};

exports.details = function (req, res) {
    OrderDetails
        .create({
            dish_id: req.body.dish_id,
            dish_name: req.body.dish_name,
            quantity: req.body.quantity,
            sub_bill: req.body.sub_bill,
            order_id: req.body.order_id
        })
        .then(function (orderDetails) {
            res.json(orderDetails);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.listAll = function (req, res) {
    Order
        .findAll({
            where: {
                buy_user_id: req.params.id
            }
        })
        .then(function (orders) {
            res.json(orders);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.payment = function(req,res){
    console.log("in orders controller - payment");
    console.log(req.body);
    var create_payment_json = {
        intent: "sale",
        payer: {
            payment_method: "credit_card",
            funding_instruments: [{
                credit_card: {
                    type: req.body.payment.cardType,
                    number: req.body.payment.no,
                    expire_month: req.body.payment.expMonth,
                    expire_year: req.body.payment.expYear,
                    cvv2: req.body.payment.cvv2,
                    first_name: req.body.payment.name_first,
                    last_name: req.body.payment.name_last,
                    billing_address: {
                        line1: req.body.payment.address,
                        city: req.body.payment.city,
                        state: req.body.payment.state,
                        postal_code: req.body.payment.postal_code,
                        country_code: req.body.payment.country
                    }
                }
            }]
        },
        transactions: [{
            amount: {
                total: req.body.transaction,
                currency: "SGD",
                details: {
                    subtotal: req.body.transaction,
                    tax: "0",
                    shipping: "0"
                }
            },
            description: ""
        }]
    };

    paypal_api.payment.create(create_payment_json, config.paypal, function (errx, resx) {
        if (errx) {
            console.log("error at paypal");
            console.log(errx);
        }

        if (resx) {
            console.log("Create Payment Response");
            // console.log(">>response: ", resx['links'][0]['href']);
            res.json({transaction:"successful"});

            Order
                .findAll({
                    where: {
                        order_no: req.params.id
                    }
                })
                .then(function(result){
                    result.forEach(function(order){
                        order.updateAttributes({
                            confirmed: "Y"
                        }).then(function (){
                            console.log("Updated order");
                            res.status(200).end();
                        }).catch(function (err){
                            console.log("Update failed");
                            handleErr(res, err);
                        });
                    })
                })
                .catch(function(err){
                    handleErr(res, err);
                });
        }
    });
};

exports.listsell = function (req, res) {
    Order
        .findAll({
            where: {
                sell_user_id: req.params.id
            },
            include: [{
                model: OrderDetails
            }]
        })
        .then(function (orders) {
            res.json(orders);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "Order not found!"});
}