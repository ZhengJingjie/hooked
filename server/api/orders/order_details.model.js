/**
 * Created by Jingjie on 7/12/16.
 */
module.exports = function (sequelize, Sequelize){
    var OrderDetails = sequelize.define('order_details', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        order_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        dish_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        dish_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        sub_bill: {
            type: Sequelize.INTEGER,
            allowNull: true
        }
    }, {
        tableName: 'order_details',
        timestamp : true
    });

    return OrderDetails;
};