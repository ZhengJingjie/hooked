/**
 * Created by Jingjie on 21/11/16.
 */
module.exports = function (sequelize, Sequelize){
    var Users = sequelize.define('users', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name_first: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name_last: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        mobile: {
            type: Sequelize.STRING,
            allowNull: true
        },
        gender: {
            type: Sequelize.STRING,
            allowNull: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: true
        },
        photo: {
            type: Sequelize.STRING,
            allowNull: true
        },
        address: {
            type: Sequelize.STRING,
            allowNull: true
        },
        postal_code: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        provider: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, {
        tableName: 'users',
        timestamp : true
    });

    return Users;
};