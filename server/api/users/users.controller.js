/**
 * Created by Jingjie on 21/11/16.
 */
var User = require("../../database").User;
var Sequelize = require("sequelize");

exports.list = function (req, res) {
    User
        .findAll({
            limit: 9,
            where: {
                provider: 'Y'
            }
        })
        .then(function (users) {
            res.json(users);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.create = function (req, res) {
    console.log("inside server controller")
    User
        .create({
            name_first: req.body.name_first,
            name_last: req.body.name_last,
            password: req.body.password,
            email: req.body.email,
            provider: req.body.provider
        })
        .then(function (user) {
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.search = function (req, res) {
    User
        .findAll({
            where: {
                address: req.params.id,
                provider: 'Y'
            }
        })
        .then(function (users) {
            res.json(users);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function(req,res){
    User
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                name_first: req.body.name_first,
                name_last: req.body.name_last,
                password: req.body.password,
                email: req.body.email,
                gender: req.body.gender,
                address: req.body.address,
                postal_code: req.body.postal_code,
                provider: req.body.provider
            }).then(function (){
                console.log("Updated profile");
                res.status(200).end();
            }).catch(function (err){
                console.log("Update failed");
                handleErr(res, err);
            });
        })
        .catch(function(err){
            handleErr(res, err);
        });
};

exports.delete = function(req,res){
    User
        .destroy({
            where: {
                id: req.params.id
            }

        })
        .then(function(result) {
            console.log("Deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            handleErr(res, err);
        })

};

exports.searchAll = function (req, res) {
    User
        .findOne({
            where: Sequelize.or(
                {id: req.params.id},
                {email: req.params.id}
            )
        })
        .then(function (user) {
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

/*
exports.update = function (req, res) {
    User
        .findById(req.params.id)
        .then(function (user) {

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

*/

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "User not found!"});
}