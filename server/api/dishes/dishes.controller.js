/**
 * Created by Jingjie on 21/11/16.
 */
var Dish = require("../../database").Dish;

exports.create = function (req, res) {
    console.log("inside server controller");
    Dish
        .create({
            dish_name: req.body.dish_name,
            description: req.body.description,
            quantity: req.body.quantity,
            price: req.body.price,
            user_id: req.body.user_id
        })
        .then(function (user) {
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.list = function (req, res) {
    Dish
        .findAll({
            where: {
                user_id: req.params.id
            }
        })
        .then(function (dishes) {
            res.json(dishes);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
}

exports.dish = function (req, res) {
    Dish
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function (dishes) {
            res.json(dishes);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.delete = function(req,res){
    Dish
        .destroy({
            where: {
                id: req.params.id
            }

        })
        .then(function(result) {
            console.log("Deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            handleErr(res, err);
        })
};

exports.update = function(req,res){
    Dish
        .findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                dish_name: req.body.dish_name,
                dish_pic: req.body.dish_pic,
                description: req.body.description,
                quantity: req.body.quantity,
                price: req.body.price
            }).then(function (){
                console.log("Updated dish");
                res.status(200).end();
            }).catch(function (err){
                console.log("Update failed");
                handleErr(res, err);
            });
        })
        .catch(function(err){
            handleErr(res, err);
        });

};

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "User not found!"});
}