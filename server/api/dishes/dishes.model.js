/**
 * Created by Jingjie on 21/11/16.
 */
module.exports = function (sequelize, Sequelize){
    var Dishes = sequelize.define('dishes', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        dish_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        dish_pic: {
            type: Sequelize.STRING,
            allowNull: true
        },
        description: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_order: {
            type: Sequelize.DATE,
            allowNull: true
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        quantity_left: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        price: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        user_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {
        tableName: 'dishes',
        timestamp : true
    });

    return Dishes;
};