/**
 * Created by Jingjie on 21/11/16.
 */
module.exports = function (sequelize, Sequelize){
    var Reviews = sequelize.define('reviews', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        buy_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        sell_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        review: {
            type: Sequelize.TEXT,
            allowNull: false
        }
    }, {
        tableName: 'reviews',
        timestamp : true
    });

    return Reviews;
};