/**
 * Created by Jingjie on 21/11/16.
 */
var Review = require("../../database").Review;
var User = require("../../database").User;

exports.create = function (req, res) {
    Review
        .create({
            buy_user_id: req.body.buy_user_id,
            sell_user_id: req.body.sell_user_id,
            review: req.body.review
        })
        .then(function (user) {
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.list = function (req, res) {
    Review
        .findAll({
            where: {
                sell_user_id: req.params.id
            },
            include: [{
                model: User
            }]
        })
        .then(function (reviews) {
            res.json(reviews);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "User not found!"});
}