/**
 * Created by Jingjie on 21/11/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("SearchCtrl", ["$http", "$state", SearchCtrl]);

console.log("in search");

    function SearchCtrl($http, $state) {
        var vm = this;

        vm.list = {};
        vm.searchValue = "";

        init();

        function init(){
            $http
                .get('/api/users')
                .then(function (response) {
                    vm.list = response.data;
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.search = function(){
            $http
                .get('/api/users/'+vm.searchValue)
                .then(function (response) {
                    vm.list = response.data;
                    console.log(response);
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.details=function (id){
            $state.go("order", {Id : id});
        }
    }

})();