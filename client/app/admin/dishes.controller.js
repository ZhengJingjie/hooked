/**
 * Created by Jingjie on 3/12/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("adminDishesCtrl", ["$http", "$stateParams", "$state", "Upload", adminDishesCtrl]);

    function adminDishesCtrl($http, $stateParams, $state, Upload) {
        var vm = this;

        vm.showUpload = false;

        var id = $stateParams.Id;
        var dishId = $stateParams.dishId;


        vm.list = {};
        vm.dish = {};

        vm.form = {file: null};

        init();

        function init(){

            // $http
            //     .get('/checkauth')
            //     .then(function (response) {
            //         if(response.data.key) {
            //             vm.list = response.data.user;
            //             console.log(vm.list);
            //         }
            //     })
            //     .catch(function(err) {
            //         console.log(err);
            //     })

            $http
                .get('/api/dish/'+dishId)
                .then(function (response) {
                    vm.dish = response.data;
                    console.log(vm.dish);
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.redirectOrders = function(){
            $state.go("adminOrders", {Id : id});
        }

        // function save(url) {
        //     $http
        //         .post("/api/posts", {
        //             url: url,
        //             caption: self.form.caption
        //         })
        //         .then(function () {
        //             console.log("Dish successfully saved");
        //         })
        //         .catch(function () {
        //             alert("Oops some error occurred.");
        //         });
        // };

        vm.prepareUpload = function() {
            $http
                .post("/api/aws/s3-policy", {
                    mimeType: vm.form.file.type
                })
                .then(function (response) {
                    uploadFile(response.data);
                    console.log(response);
                    //console.log(afterUpload)
                })
                .catch(function (response) {

                });
        }

        function uploadFile(fileUploadConfig) {

            console.log(fileUploadConfig.bucketUrl);

            vm.dateNow = Date.now();
            vm.key = vm.dateNow + "_" + vm.form.file.name;

            Upload.upload({
                url: fileUploadConfig.bucketUrl,
                method: 'POST',
                data: {
                    key: vm.key,
                    AWSAccessKeyId: fileUploadConfig.AWSAccessKeyId,
                    acl: "public-read",
                    policy: fileUploadConfig.s3Policy,
                    signature: fileUploadConfig.s3Signature,
                    "Content-Type": vm.form.file.name,
                    filename: vm.form.file.name,
                    file: vm.form.file
                }
            }).then(function (resp) {
                vm.dish.dish_pic = "https://s3-ap-southeast-1.amazonaws.com/jingjie/"+vm.key;
                //picUrl(resp.config.data.key);
                console.log("before broadcast !");
                //$rootScope.$broadcast('updateList');
            }, function (resp) {
                console.log(resp);
            }, function (evt) {
                console.log(evt);
            });
        }

        vm.back = function(){
            $state.go("adminMain", {Id : id});
        }

        vm.delete = function(){
            $http
                .delete('/api/dishes/'+dishId)
                .then(function (response) {
                    console.info("Deleted");
                    console.info(response);
                })
                .catch(function (err) {
                    console.log(err);
                    console.info("Delete failed")
                })
        }

        vm.update = function(){
            $http
                .put('/api/dishes/'+dishId, vm.dish)
                .then(function (response) {
                    console.info("Updated dish");
                    console.info(response);
                })
                .catch(function (err) {
                    console.log(err);
                    console.info("Update failed")
                })
        }
    }

})();