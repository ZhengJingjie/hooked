/**
 * Created by Jingjie on 4/12/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("adminOrdersCtrl", ["$http", "$stateParams", "$state", adminOrdersCtrl]);

    function adminOrdersCtrl($http, $stateParams, $state) {
        var vm = this;

        var id = $stateParams.Id;

        vm.listOrders = {};

        init();

        function init(){

            // $http
            //     .get('/checkauth')
            //     .then(function (response) {
            //         if(response.data.key) {
            //             vm.list = response.data.user;
            //             console.log(vm.list);
            //         }
            //     })
            //     .catch(function(err) {
            //         console.log(err);
            //     })

            $http
                .get('/api/sellOrders/'+$stateParams.Id)
                .then(function (response) {
                    vm.listOrders = response.data;
                    console.log(vm.listOrders);
                })
                .catch(function (err) {
                    console.log(err);
                })

        }

        vm.redirectMain = function(){
            $state.go("adminMain", {Id : id});
        }

    }

})();