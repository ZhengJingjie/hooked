/**
 * Created by Jingjie on 2/12/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("adminMainCtrl", ["$http", "$stateParams", "$state", adminMainCtrl]);

    function adminMainCtrl($http, $stateParams, $state) {
        var vm = this;

        var id = $stateParams.Id;

        vm.listDishes = {};
        vm.listReviews = {};
        vm.dish = {};

        init();

        function init(){

            // $http
            //     .get('/checkauth')
            //     .then(function (response) {
            //         if(response.data.key) {
            //             vm.list = response.data.user;
            //             console.log(vm.list);
            //         }
            //     })
            //     .catch(function(err) {
            //         console.log(err);
            //     })

            $http
                .get('/api/dishes/'+id)
                .then(function (response) {
                    vm.listDishes = response.data;
                    console.log(vm.listDishes);
                })
                .catch(function (err) {
                    console.log(err);
                })

            $http
                .get('/api/reviews/'+id)
                .then(function (response) {
                    vm.listReviews = response.data;
                    console.log(vm.listReviews);
                })
                .catch(function (err) {
                    console.log(err);
                })

        }

        vm.redirectOrders = function(){
            $state.go("adminOrders", {Id : id});
        }

        vm.details = function (dishid){
            $state.go("adminDishes", {Id : id, dishId : dishid});
        }

        vm.addDish = function (){
            console.log(vm.dish.dish_name);
            $http
                .post('/api/dishes', {
                    dish_name: vm.dish.dish_name,
                    description: vm.dish.description,
                    quantity: vm.dish.quantity,
                    price: vm.dish.price,
                    user_id: id
                })
                .then(function (response) {
                    console.log("successfully added");
                    init();
                }).catch(function (err) {
                console.log(err);
            })
        }

    }

})();