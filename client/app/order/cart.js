/**
 * Created by Jingjie on 6/12/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("CartCtrl", ["$http", "$stateParams", "$state", CartCtrl]);
    console.log("in order");

    function CartCtrl($http, $stateParams, $state) {
        var vm = this;

        vm.listUsers = {};
        vm.listOrders = {};

            init();

        function init(){
            $http
                .get('/api/allUsers/'+$stateParams.Id)
                .then(function (response) {
                    vm.listUsers = response.data;
                    console.log(vm.listUsers);
                })
                .catch(function (err) {
                    console.log(err);
                })

            $http
                .get('/api/orders/'+$stateParams.Id)
                .then(function (response) {
                    vm.listOrders = response.data;
                    console.log(vm.listOrders);
                    vm.listOrders.forEach(function(status){
                        if(status.confirmed == "Y") {
                            status.confirmed = "Confirmed"
                        }else {
                            status.confirmed = "Not Paid"
                        };
                        if(status.completed == "Y") {
                            status.completed = true;
                        }else {
                            status.completed = false;
                        };
                    })
                    console.log(vm.listOrders);
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.checkout = function() {
            $state.go("payment", {buyId: vm.listUsers.id, order: vm.listOrders[0].order_no});
        }

        vm.complete = function() {
            $http
                .put('/api/order/'+vm.listOrders[0].id, {
                    completed: "Y"
                })
                .then(function (response) {
                    console.info("Transaction completed")
                })
                .catch(function (err) {
                    console.log(err);
                    console.info("Update failed")
                })
        }
    }

})();