/**
 * Created by Jingjie on 22/11/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("OrderCtrl", ["$http", "$stateParams", "$state", OrderCtrl]);
    console.log("in order");

    function OrderCtrl($http, $stateParams, $state) {
        var vm = this;

        vm.selectedDish = "";
        vm.selectedDishId = "";
        vm.quantity = "";

        vm.list = {};
        vm.listUsers = {};
        vm.listDishes = {};
        vm.listReviews = {};
        vm.newReview = "";

        vm.oneOrder = {};
        vm.cartItems = [];
        vm.price = 0;
        vm.subBill = 0;
        vm.totalBill = 0;
        vm.orderNo = "";
        vm.orderId = "";

        init();

        function init(){
            $http
                .get('/api/allUsers/'+$stateParams.Id)
                .then(function (response) {
                    vm.listUsers = response.data;
                    console.log(vm.listUsers);
                })
                .catch(function (err) {
                    console.log(err);
                })


            $http
                .get('/api/dishes/'+$stateParams.Id)
                .then(function (response) {
                    vm.listDishes = response.data;
                    console.log(vm.listDishes);
                })
                .catch(function (err) {
                    console.log(err);
                })

            $http
                .get('/api/reviews/'+$stateParams.Id)
                .then(function (response) {
                    vm.listReviews = response.data;
                    console.log(vm.listReviews);
                })
                .catch(function (err) {
                    console.log(err);
                })

            $http
                .get('/checkauth')
                .then(function (response) {
                    console.log("checkauth responded");
                    console.log(response.data.key);
                    if(response.data.key) {
                        vm.list = response.data.user;
                        console.log(vm.list);
                    }
                })
                .catch(function(err) {
                    console.log(err);
                })
        }

        vm.review = function(){
            $http
                .post('/api/reviews', {
                    buy_user_id: vm.list.id,
                    sell_user_id: vm.listUsers.id,
                    review: vm.newReview
                })
                .then(function (response) {
                    console.log("successfully posted");
                    init();
                })
                .catch(function (err) {
                console.log(err);
            })
        }

        vm.addToCart = function(){
            vm.listDishes.forEach(function(dish){
                if (vm.selectedDish == dish.dish_name){
                    vm.selectedDishId = dish.id;
                    vm.price = dish.price;
                    vm.subBill = vm.price*vm.quantity;
                }
            })

            vm.totalBill=vm.totalBill+vm.subBill;

            vm.oneOrder = {};

            vm.oneOrder.dishId = vm.selectedDishId;
            vm.oneOrder.dish = vm.selectedDish;
            vm.oneOrder.quantity = vm.quantity;
            vm.oneOrder.subBill = vm.subBill;
            vm.cartItems.push(vm.oneOrder);
        }

        vm.deleteItem = function($index){
            vm.cartItems.splice($index, 1);
        }

        vm.payment=function (){
            vm.orderNo = Date.now()  + vm.list.id + vm.listUsers.id;
            $state.go("payment", {buyId: vm.list.id, order: vm.orderNo});

            console.log(vm.orderNo);
            console.log(Date.now());

            $http
                .post('/api/order/'+vm.orderNo, {
                    order_no: vm.orderNo,
                    buy_user_id: vm.list.id,
                    sell_user_id: vm.listUsers.id,
                    total_bill: vm.totalBill,
                    confirmed: "N",
                    completed: "N"
                })
                .then(function (response) {
                    console.log("successfully ordered");
                    vm.orderId = response.data.id;

                    vm.cartItems.forEach(function(order) {
                        $http
                            .post('/api/orders/'+vm.orderId, {
                                order_id: vm.orderId,
                                dish_id: order.dishId,
                                dish_name: order.dish,
                                quantity: order.quantity,
                                sub_bill: order.subBill
                            })
                            .then(function (response) {
                                console.log("successfully ordered");
                            })
                            .catch(function (err) {
                                console.log(err);
                            })
                    })
                })
                .catch(function (err) {
                    console.log(err);
                })

            // orderService.setOrder(vm.cartItems);
        }
    }

})();

