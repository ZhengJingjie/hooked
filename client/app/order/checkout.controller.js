/**
 * Created by Jingjie on 5/12/16.
 */
(function () {
    angular
        .module("myApp")

        .controller("CheckoutCtrl", ["$http", "$stateParams", "$state", "ngProgressFactory", CheckoutCtrl]);

    function CheckoutCtrl($http, $stateParams, $state, ngProgressFactory) {
        var vm = this;

        vm.progressbar = ngProgressFactory.createInstance();

        vm.orderNo = "";
        vm.totalBill = "";
        vm.buyId = "";
        vm.listOrders = [];
        vm.listUsers = {};
        vm.card = {};

        vm.buyId = $stateParams.buyId;
        vm.orderNo = $stateParams.order;

        vm.month = [01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12];
        vm.year = [2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025];

        // vm.cartItems = orderService.getOrder();

        init();

        function init(){
            $http
                .get('/api/order/'+vm.orderNo)
                .then(function (response) {
                    console.log(response);
                    vm.listOrders = response.data;
                })
                .catch(function (err) {
                    console.log(err);
                })

            $http
                .get('/api/allUsers/'+vm.buyId)
                .then(function (response) {
                    vm.listUsers = response.data;
                    vm.listUsers.country = "SG";
                    vm.listUsers.city = "Singapore";
                    vm.listUsers.state = "Singapore";
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.purchase = function() {
            vm.progressbar.start();
            console.log(vm.listOrders[0].total_bill);
            $http
                .put('/api/orders/'+vm.orderNo, {
                    transaction: vm.listOrders[0].total_bill,
                    payment: vm.listUsers
                })
                .then(function (response) {
                    console.log("successfully paid");
                    vm.progressbar.complete();
                    $state.go("search");
                }).catch(function (err) {
                console.log(err);
                vm.progressbar.stop();
            })
        }

    }

})();