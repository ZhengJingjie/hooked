/**
 * Created by Jingjie on 21/11/16.
 */

(function () {
    angular
        .module("myApp", ["ui.router", "ngFlash", "ngFileUpload", "ngProgress"]);

})();