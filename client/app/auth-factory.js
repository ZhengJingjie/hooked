/**
 * Created by Jingjie on 29/11/16.
 */
(function () {
    angular.module('myApp').factory('AuthFactory',
        ['$q', '$timeout', '$http', '$state', 'Flash',
            function ($q, $timeout, $http, $state, Flash) {
                var user = null;
                return ({
                    isLoggedIn: isLoggedIn,
                    getUserStatus: getUserStatus,
                    login: login,
                    logout: logout
                });

                function isLoggedIn() {
                    if(user) {
                        console.log("user");
                        console.log(user);
                        return true;
                    } else {
                        return false;
                    }
                }

                function getUserStatus(callback) {
                    $http.get('/status/user')
                        .then(function (data) {
                            var authResult = JSON.stringify(data);
                            if(data["data"] != ''){
                                user = true;
                                callback(user);
                            } else {
                                user = false;
                                callback(user);
                            }
                        });
                }

                function login(userProfile) {
                    var deferred = $q.defer();

                    console.log("userProfile");
                    console.log(userProfile);
                    $http.post('/login', userProfile)
                        .then(function (response) {
                            console.log("login successs");
                            console.log(response.data);
                            console.log(response.status);
                            console.log(userProfile);

                            if(response.status == 200){
                                getUserStatus(function(result){
                                    if(result){
                                        console.log(result);
                                        deferred.resolve(response.data);
                                    }else{
                                        deferred.reject("user status error");
                                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                                    }
                                });
                            } else {
                                user = false;
                                Flash.clear();
                                Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                                deferred.reject("resposne status error");
                            }
                        })
                        .catch(function (err) {
                            console.log("error");
                            console.log(err);
                            user = false;
                            Flash.clear();
                            Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                            deferred.reject(err);
                        });

                    return deferred.promise;

                }

                function logout() {

                    var deferred = $q.defer();

                    $http.get('/logout')
                        .success(function (data) {
                            user = false;
                            deferred.resolve();
                        })
                        .error(function (data) {
                            user = false;
                            deferred.reject();
                        });

                    return deferred.promise;

                }

            }]);

})();