/**
 * Created by Jingjie on 27/11/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("ProfileCtrl", ["$http", "$stateParams", ProfileCtrl]);

    function ProfileCtrl($http, $stateParams) {
        var vm = this;

        vm.list = {};

        init();

        function init(){
            $http
                .get('/api/allUsers/'+$stateParams.Id)
                .then(function (response) {
                    vm.list = response.data;
                    console.log(vm.list);
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        vm.update = function(){
            $http
                .put('/api/users/'+vm.list.id, vm.list)
                .then(function (response) {
                    console.info("Updated profile")
                })
                .catch(function (err) {
                    console.log(err);
                    console.info("Update failed")
                })
        }

        vm.deleteAccount = function(){
            $http
                .delete('/api/users/'+vm.list.id, vm.list)
                .then(function (response) {
                    console.info("Deleted")
                })
                .catch(function (err) {
                    console.log(err);
                    console.info("Delete failed")
                })
        }

    }

})();