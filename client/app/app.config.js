/**
 * Created by Jingjie on 21/11/16.
 */
(function () {
    angular
        .module("myApp")
        .config(AppConfig);

    AppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function AppConfig($stateProvider,$urlRouterProvider){

        console.log("inside the app.config");

        $stateProvider
            .state('search',{
                url : '/search',
                templateUrl :'./app/search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('order',{
                url : '/order/:Id',
                templateUrl :'./app/order/order.html',
                controller : 'OrderCtrl',
                controllerAs : 'ctrl'
            })
            .state('profile',{
                url : '/profile/:Id',
                templateUrl :'./app/profile/profile.html',
                controller : 'ProfileCtrl',
                controllerAs : 'ctrl'
            })
            .state('adminMain',{
                url : '/provider/adminMain/:Id',
                templateUrl :'./app/admin/admin.main.html',
                controller : 'adminMainCtrl',
                controllerAs : 'ctrl'
            })
            .state('adminDishes',{
                url : '/provider/adminMain/:Id/Dishes/:dishId',
                templateUrl :'./app/admin/admin.dishes.html',
                controller : 'adminDishesCtrl',
                controllerAs : 'ctrl'
            })
            .state('adminOrders',{
                url : '/provider/adminOrders/:Id',
                templateUrl :'./app/admin/admin.orders.html',
                controller : 'adminOrdersCtrl',
                controllerAs : 'ctrl'
            })
            .state('cart',{
                url : '/cart/:Id',
                templateUrl :'./app/order/cart.html',
                controller : 'CartCtrl',
                controllerAs : 'ctrl'
            })
            .state('payment',{
                url : '/payment/:buyId/:order',
                templateUrl :'./app/order/checkout.html',
                controller : 'CheckoutCtrl',
                controllerAs : 'ctrl'
            });

        $urlRouterProvider.otherwise("/search");
    }
})();