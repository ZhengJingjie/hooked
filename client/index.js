/**
 * Created by Jingjie on 24/11/16.
 */
(function () {
    angular
        .module("myApp")
        .controller("IndexCtrl", ["$http", "$state", "AuthFactory", IndexCtrl]);

    console.log("in index");

    function IndexCtrl($http, $state, AuthFactory) {
        console.log("in index ctrl");
        var vm = this;

        vm.list = {};
        vm.boolean = false;
        vm.confirmPassword = "";

        init();

        function init() {
            console.log("in init");
            $http
                .get('/checkauth')
                .then(function (response) {
                    if(response.data.key) {
                        vm.boolean = true;
                        vm.list = response.data.user;
                        console.log("in init then")
                    }
                })
                .catch(function(err) {
                    console.log("in init catch");
                    console.log(err);
                })
        }

        // getUserName();
        //
        // function getUserName() {
        //     $http
        //         .get('/api/allUsers/'+vm.list.email)
        //         .then(function (response) {
        //             vm.list = response.data;
        //         })
        //         .catch(function (err) {
        //             console.log(err);
        //         })
        // }
        vm.home = function(){
            $state.go("search");
        }

        vm.details=function (id) {
            $state.go("profile", {Id : id});
        }

        vm.cart=function (id) {
            $state.go("cart", {Id : id});
        }

        vm.admin=function (id) {
            $state.go("adminMain", {Id : id});
        }

        vm.login = function () {
            AuthFactory
                .login(vm.list)
                .then(function (response) {
                    if(AuthFactory.isLoggedIn()){
                        console.log("in index controller - login");
                        vm.list = response;
                        console.log(response);
                        console.log(vm.list);
                        vm.boolean = AuthFactory.isLoggedIn();
                        // vm.list.email = "";
                        // vm.list.password = "";
                        // vm.list.gender = "";
                        // vm.list.birth_date = "";
                        // vm.list.address= "";
                        // vm.list.postal_code = "";
                    }else{
                        console.error("Error logging on !");
                    }
                }).catch(function () {
                console.error("Error logging on !");
            });
        }

        vm.logout = function () {
            AuthFactory.logout()
                .then(function () {
                    console.log("Logout");
                    vm.boolean = false;
                }).catch(function (err) {
                console.log(err);
            });
        };

        vm.signUp = function(){

            if(vm.list.password == vm.confirmPassword){
                console.log("inside sign up controller")
                $http
                    .post('/api/users', {
                        name_first: vm.list.name_first,
                        name_last: vm.list.name_last,
                        password: vm.list.password,
                        email: vm.list.email,
                        provider: "N"
                    })
                    .then(function (response) {
                        console.log("successfully registered");
                        vm.list.password = "";
                        vm.confirmPassword= "";
                    }).catch(function (err) {
                    console.log(err);
                    })
            }else{
                console.log("password mis-match")
            }

        }

        vm.details=function (id) {
            $state.go("profile", {Id : id});
        }

        // vm.details = function(userId, userEmail, userName1, userName2){
        //     $state
        //         .go("profile", {Id:userId});
        //
        //     var userObj = {};
        //     userObj.user_id = userId;
        //     userObj.email = userEmail;
        //     userObj.name_first = userName1;
        //     userObj.name_last = userName2;
        //
        //     searchService.setUser(userObj);
        // }

    }

})();